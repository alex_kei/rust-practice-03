use std::collections::HashMap;

pub fn get_mode(number_sequence: &Vec<u16>) -> Option<u16> {
    if is_empty(number_sequence) {
        None
    } else {
        let number_quantities = get_quantities(number_sequence);

        let mut mode = number_sequence[0];
        let mut max_quantity: usize = 0;
        for (number, quantity) in number_quantities.iter() {
            if *quantity > max_quantity {
                max_quantity = *quantity;
                mode = *number;
            }
        }

        Some(mode)
    }
}

pub fn get_median(number_sequence: &Vec<u16>) -> Option<f32> {
    if is_empty(number_sequence) {
        None
    } else {
        let sorterd_sequence = sort(number_sequence);

        let sequence_length = sorterd_sequence.len();

        let median: f32 = if is_even(sequence_length) {
            let pre_middle_index = sequence_length / 2 - 1;
            let post_middle_index = pre_middle_index + 1;

            (sorterd_sequence[pre_middle_index] as f32
                + sorterd_sequence[post_middle_index] as f32) / 2 as f32
        } else {
            let middle_index = sequence_length / 2;

            sorterd_sequence[middle_index] as f32
        };

        Some(median)
    }
}

pub fn get_average(number_sequence: &Vec<u16>) -> Option<f32> {
    if is_empty(number_sequence) {
        None
    } else {
        let mut sum: u16 = 0;
        for number in number_sequence {
            sum += number;
        }

        let average: f32 = (sum as f32) / (number_sequence.len() as f32);

        Some(average)
    }
}

fn get_quantities(number_sequence: &Vec<u16>) -> HashMap<u16, usize> {
    let mut number_quantities: HashMap<u16, usize> = HashMap::new();

    for number in number_sequence {
        let quantity = number_quantities.entry(*number).or_insert(0);
        *quantity += 1;
    }

    number_quantities
}

fn sort(number_sequence: &Vec<u16>) -> Vec<u16> {
    let mut sequence_copy = number_sequence.clone();
    sequence_copy.sort();

    sequence_copy
}

fn is_even(number: usize) -> bool {
    if number % 2 == 0 { true } else { false } 
}

fn is_empty(number_sequence: &Vec<u16>) -> bool {
    if number_sequence.len() > 0 { false } else { true }
}
