mod random_list;
mod statistics;

const MIN: u16 = 1;
const MAX: u16 = 100;
const LENGTH: usize = 256;

fn main() {
    let number_sequence: Vec<u16> = random_list::create(MIN, MAX, LENGTH);

    println!("Sequence: {:?}", number_sequence);
    println!("Length: {}", number_sequence.len());

    if let Some(mode) = statistics::get_mode(&number_sequence) {
        println!("Sequence mode is {}", mode);
    }
    if let Some(median) = statistics::get_median(&number_sequence) {
        println!("Sequence median is {}", median);
    }
    if let Some(average) = statistics::get_average(&number_sequence) {
        println!("Sequence average is {}", average);
    }
}
