use rand::Rng;

pub fn create(min: u16, max: u16, length: usize) -> Vec<u16> {
    let mut generator = rand::thread_rng();
    let mut number_list: Vec<u16> = Vec::new();

    for _ in 0..length {
        let number: u16 = generator.gen_range(min..max);
        number_list.push(number);
    }

    number_list
}
